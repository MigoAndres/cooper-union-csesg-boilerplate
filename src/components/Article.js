import React, { Component } from "react";
import axios from "axios";
import Header from '../pages/Header';
import TopBar from '../pages/topbar';
import NavBar from '../pages/navbar';

export default class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: {}
    };
  }

  componentDidMount() {
    axios
      .get(
        "http://public-api.wordpress.com/rest/v1/sites//cfotsl495492035.wordpress.com/posts/" +
          this.props.match.params.id
      )
      .then(res => {
        console.log(res.data);
        this.setState({ post: res.data });
      })
      .catch(error => console.log(error));
  }

  parseOutScripts(content) {}

  render() {

    if (this.state.post) { 
      //console.log(this.removeUnicode(this.state.post.title))


      return (
        <div className="blog">
          <div className="article">
            <Header />
            <TopBar />
            <div style = {{marginTop: 60, marginBottom: 100, width: '70%', marginLeft: 40}}>
              {this.state.post.featured_image ? (
                <img
                  className="img-responsive webpic"
                  alt="article header"
                  src={this.state.post.featured_image}
                />
              ) : (
                ""
              )}
              <h1 className="text-center" dangerouslySetInnerHTML={{ __html: this.state.post.title }}></h1>
              <div
                className="content"
                dangerouslySetInnerHTML={{ __html: this.state.post.content }}
              />

               </div>
              <NavBar style = {{marginTop: 70}}/>
           
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
