import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { Column, Row } from 'simple-flexbox';
import { withStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Menu from '@material-ui/core/Menu';
import Hidden from '@material-ui/core/Hidden';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Paper from '@material-ui/core/Paper';
import { createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Slideshow from './slideshow';
import Header from './Header';
import TopBar from './topbar';
import NavBar from './navbar';
import HomeNews from './homenews';
import Homeaboutus from './homeaboutus';
import background from '../assets/background.jpg';
import stats from './stats';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});

class HomePage extends React.Component{
    state = {
    	spacing: '16',
    };

    handleChange = key => (event, value) => {
    	this.setState({
    		[key]: value,
    	});
    };

    render()
    {
    	const {classes} = this.props;
    	const {spacing} = this.state;
         const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: {
      main: '#f44336',
    },
  },
});	

		return (
	    <div id = "home">
		    		<Header />
		    		<TopBar />

	    		<center style={{fontSize: 50}}> <b> Future Heroes </b> </center>

				
				<Slideshow style={{backgroundImage: 'url('+background+')'}}/>

				<div> 
					<Paper id="card" style = {{marginBottom: 15, maxWidth: 2000}}>
				        <h1> Our Mission: </h1>
				        	<p> 
				        		Our goal is to foster community amongst the children and youth within and outside Sierra Leone by alleviating their suffering generally in difficult circumstances.
				        	</p>        
					</Paper>

					<Row>
					<Column>
					    <Row>
					        <Paper id="card" style = {{marginBottom: 15, maxWidth: 1000}}>	
			        			<ul> <h1> Why help Sierra Leone? </h1> </ul>
			        			<p>               
									<ul>
									- Of those age 15 and over, only 48.1% can read and write English, Mende, Temne, or Arabic
									</ul>
									<ul>
									- The child labor rate (children ages 14 and under) is 48% 
									</ul>
									<ul>
									- The percent of underweight children under the age of 5 is 18.1%
									</ul>
									<ul>
									- There are 0.02 physicians and 0.4 hospital beds for every 1000 people
									</ul>
									<ul>
									-  More than 60% of the population is living on less than 1.25 USD a day
									</ul>
									<ul>
									- The Civil war destroyed schools and left thousands homeless 
									</ul>
			        			</p>
				        	</Paper>
					    </Row>
				    </Column> 

				    <Column>
				        <Paper id="card" style = {{marginBottom: 15, maxWidth: 1000}}>
		        			<ul> <h1> About Us </h1> </ul>
			        			<ul> 
			        				Our motto is "Future Heroes." 
			        				The Children's Foundation of Technology is a nonprofit organization that strives to support the youth in Sierra Leone. 
			        				We want to help the less priveleged children and teach them technological and media skills so they don't have to break rocks for a living like their elders. 
			        				The Children's Foundation of Technology wants to help the children grow up to be self-dependant adults. 
			        				To know more, press Read More.
			        			</ul>
			        			<Link to = "/about" style= {{color: 'black',textDecoration:'none' }}>
			                        <Tab label = "Read More" />
			                    </Link>     	
		          		</Paper>
				    </Column>
				    </Row>

		          	<Paper id="card" style = {{marginBottom: 25, maxWidth: 2000}}>
		        			<ul> <h1> News </h1> </ul>
			        			<ul> 
			        				Click here to learn about recent events occurring in Sierra Leone.
			        			</ul>
		        			<Link to = "/news" style= {{color: 'black',textDecoration:'none' }} />
		          	</Paper>
		        </div>

			<NavBar />
		</div>

		)
    }
};

export default HomePage
