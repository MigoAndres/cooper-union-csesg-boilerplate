import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';
import Hidden from '@material-ui/core/Hidden';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import MenuList from '@material-ui/core/MenuList';
import Popper from '@material-ui/core/Popper';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import PropTypes from 'prop-types';


class TopBar extends React.Component{

	state = {
    	anchorEl: null,
    	open: false,
  		};

  	handleClick = event => {
    	this.setState({ anchorEl: event.currentTarget });
 							 };

  	handleClose = () => {
   		this.setState({ anchorEl: null });
 	 };

 	handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
 	 };

 	 handleCloseMenu = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
 	 };



	render()
	{
		const { anchorEl } = this.state;
		const { open } = this.state;
		return(

			<div id = "topNav">
					<Hidden xsDown>
						<AppBar position="static" style = {{backgroundColor: 'black', marginBottom: 20, flexGrow: 1,display: "flex", textAlign: "center"}} >
							<Tabs>
								<Grid container spacing={48}>
									<Grid item xs={12}>
									    <Link to="/about" style= {{color: 'white',textDecoration:'none'}}>
						            		<Tab label = "About" />
						            	</Link>
						           

						        	
									    <Link to="/news" style= {{color: 'white',textDecoration:'none'}}>
						            		<Tab label = "News" />
						            	</Link>
						            
						            	<Link to="/blog" style= {{color: 'white',textDecoration:'none' }}>
						            		<Tab label = "Blog" />
						            	</Link>
						            
						            	<Button
							            buttonRef={node => {
							              this.anchorEl = node;
							            }}
							            aria-owns={open ? 'menu-list-grow' : null}
							            aria-haspopup="true"
							            onClick={this.handleToggle}
							            style = {{color: 'white', margin: 'auto', opacity: 0.7}}
							          	>
							           	 Gallery
							          	</Button>
							          	<Popper open={open} anchorEl={this.anchorEl} transition >
							            	{({ TransitionProps, placement }) => (
							              	<Grow
							                	{...TransitionProps}
							                	id="menu-list-grow"
							                	style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
							              	>
							                	<Paper>
							                  	<ClickAwayListener onClickAway={this.handleCloseMenu}>
							                    	<MenuList>

							                      	<MenuItem onClick={this.handleCloseMenu}>
							                      		<Link to="/photos" style= {{color: 'black',textDecoration:'none' }}>
						            						<Tab label = "Photos" />
						            					</Link>
							                      	</MenuItem>

							                      	<MenuItem onClick={this.handleCloseMenu}>
							                      		<Link to="/videos" style= {{color: 'black',textDecoration:'none' }}>
						            						<Tab label = "Videos" />
						            					</Link>
							                      	</MenuItem>

							                    	</MenuList>
							                  	</ClickAwayListener>
							                	</Paper>
							              	</Grow>
							            	)}
							          	</Popper>
							        	        	
									
									   	<Link to="/partners" style= {{color: 'white',textDecoration:'none' }}>
						            		<Tab label = "Our Partners" />
						            	</Link>
						            
						            
						            	<Link to="/how_to_help" style= {{color: 'white',textDecoration:'none' }}>
						            		<Tab label = "How To Help" />
						            	</Link>
						            
									    <Link to = "/contact" style= {{color: 'white',textDecoration:'none' }}>
						            		<Tab label = "Contact" />
						            	</Link>
						            </Grid>
								
								</Grid>
							</Tabs>
						</AppBar>
					</Hidden>	            		

					<Hidden smUp>
							<AppBar position="static" style = {{backgroundColor: 'black', marginBottom: 20, marginTop: 25, flexGrow: 1,display: "flex"}} >
									<IconButton color="inherit" aria-label="Menu" onClick={this.handleClick}>
			             			 	<MenuIcon />
			          			  	</IconButton>
			          			
							       <Menu
							          id="simple-menu"
							          anchorEl={anchorEl}
							          open={Boolean(anchorEl)}
							          onClose={this.handleClose}
							        >

							         
							<MenuItem onClick={this.handleClose}>
							    <Link to="/about" style= {{color: 'black', textDecoration:'none'}}>
					            	<Tab label = "About" />
					            </Link>
							</MenuItem>

							<MenuItem onClick={this.handleClose}>
							    <Link to="/news" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = "News" />
					            </Link>
							</MenuItem>

							<MenuItem onClick={this.handleClose}>
							    <Link to="/photos" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = "Photos" />
					            </Link>
							</MenuItem>

							<MenuItem onClick={this.handleClose}>
							    <Link to="/videos" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = "Videos" />
					            </Link>
							</MenuItem>

							<MenuItem onClick={this.handleClose}>
							    <Link to="/partners" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = " Our Partners" />
					            </Link>
							</MenuItem> 

							<MenuItem onClick={this.handleClose}>
							    <Link to= "/how_to_help" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = "How To Help" />
					            </Link>
							</MenuItem>

							<MenuItem onClick={this.handleClose}>
							    <Link to="/volunteer" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = "Volunteer" />
					            </Link>
							</MenuItem>

							<MenuItem onClick={this.handleClose}>
							    <Link to = "/contact" style= {{color: 'black',textDecoration:'none' }}>
					            	<Tab label = "Contact" />
					            </Link>
							</MenuItem>

						</Menu>
						</AppBar>
					</Hidden>
				</div>	


			)
	}
}



export default TopBar;

