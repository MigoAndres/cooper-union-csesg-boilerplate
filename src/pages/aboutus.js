import React from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';


import Header from './Header';
import TopBar from './topbar';
import Navbar from './navbar';

class AboutPage extends React.Component {
	render() {
		return (
      <div>
        <Header />
        <TopBar />
<Grid container spacing={24} >
        <Grid item xs={12} style={{marginRight:50, marginLeft:50}}> 
	<center style={{fontWeight: 'bold', fontSize: 21}} >
	<h1> About Us </h1>
	</center>
            <h2>
              Who we are:
            </h2>

            <p>
               The Children's Foundation of Technology of Sierra Leone (CFoTSL) is a voluntary non-profit, non-sectarian, non-racial, non- political organization and shall have powers to affiliate nationally and internationally with organizations that have similar aims and objectives. This organization was founded on the 4th January 2014 by Mr. Mendy Kanu in Freetown, Sierra Leone and Mr. Adam Beard in Ohio, USA to help encourage and promote the lives of children and youth in helping to build high capacity in promoting their communities and the country as a whole through I.T and Digital Media Technology.  The Children's Foundation of Technology(CFoT)- Sierra Leone is working with the goal of helping children and youth for their future career in different communities across the nation. This Non- Governmental organization is also working to help minimize the high level of illiteracy in Sierra Leone through I. T and Digital Media Technology.  Also, we are working to help support less privileged children and youth with I.T, Tele-collaborative, and Digital Media Skills.
            </p>

            <h2>
            Our Aim:
            </h2>
            <p>
              The main aim/goal of the organization is to foster relationships amongst children and youth within and outside Sierra Leone by enhancing the full participation of children in development process in Sierra Leone and to alleviate suffering amongst children and youth generally in difficult circumstances.
              We want to make sure that the children don't have to break rocks for a living when they grow older. After all, splitting $1.50 for a day's work of breaking rocks between three men is not a good way to make a living.
            </p>
            <h2>
              Objectives:
            </h2>
        <ul>
            <li> To promote and develop understanding amongst children and youth by the elimination of all form of conflict and work towards promoting peace and stability.</li>
            <li> To render help to every child in difficult circumstance i.e. giving relief </li>
            <li> To facilitate and set up skills training centers for children and youth to be empowered </li>
            <li> To encourage and strengthen children socially and economically, so they can be self-reliant when they grow up. </li>
            <li> To help them showcase their talent in our Talent Exposure each year </li>
            <li> To promote sports, community recreation, and play facilities </li>
            <li> To promote the health and well-being of children regardless of age, ethnicity, ability, sex, or belief/political affiliation, so that they can become responsible in our society with their creative ideas as our future leaders. </li>
         </ul>

          
          <h2> 
            Meet Mendy Kanu
          </h2>

          <p> Hello. I am a Sierra Leonean.  I was born on the 15th July 1991, in a small village called Makathy
              village.
              Makathy is a small village in Bombali District in the northern part of Sierra Leone where my
              life began. I was only living with my mother N’Yadiba Kalokoh, one brother, and two
              sisters. My father, at that time, was living in Freetown, the capital of Sierra Leone. He was a
              newsvendor, and sold newspapers in Freetown. After some time, my father decided to come
              and take me from the village to live with him in Freetown and continue my education there.
              My eldest sister and brother were already with him in Freetown, I was 6-7.
              My youngest sister remained in the village with my mum. I first attended the Makathy primary
              school in the village. When I was taken to the city, I attended the Ephraim J Robinson
              Municipal School. I went there from grade 3-6. In grade 6, I took the national public exam
              and scored the highest grade in school.</p>
              <p>
              I granted a one-year scholarship to St. Edwards Secondary School in King Tom, Freetown. I
              stayed in this school for six more years getting education. It was not easy for me to get a
              secondary education. Education is valued here, but not free. My first year was free because
              of my scholarship, but the remaining years I had to pay. It was a very difficult for me to get
              money. It was there where I was noticed for my talent of music and singing. I won a price for
              singing in a telecommunications music show. I came in second and won money. It was hard
              to get this money but it was a little to help with my education. Soon other people noticed me
              and started calling upon me to do audio voice for shows, singing jingles, and doing music for
              businesses. I was able to make a little money to help pay my education. After 4 years of
              secondary school, I learn about iEARN projects and started working with the internet.
              Learning with computers was free in Sierra Leone, so I continued my secondary education
              there. I met a man named Adam Beard from the USA and he encouraged me to learn more
              with internet. He supports me financially so I was able to attend a college in Sierra Leone to
              learn and do certificate course in I. T. When I finished the course in the college, he asked me
              what I want to do and encouraged me to follow my dreams to work and teach
              underprivileged children and youth in Sierra Leone through I.T and Digital Media. I wanted to
              try to change the future for children and youth here, those that are deprived and had a life like
              mine. However, I studied I.T at the I.C.T Training Complex (College).</p>
              <p>
              Sierra Leone has been a war zone for many years. Many children were forced to help in the
              war by being soldiers. The rebels have killed many people in Sierra Leone. The war has
              affected everyone who lives here. I lost my father when I was a young boy during the war.
              We were in a hiding zone during the rebel attack and my father had to go out to get us food;
              while he was gone, he then was attacked by the rebel and killed. My brother, sister, and I had to
              fend for ourselves and try to survive. My sister returned to the village a short time later, but
              my brother and I stayed in Freetown. My brother worked by selling newspapers but that was not
              enough to buy food for both of us. I had to do bobby jobs (odd jobs) for people to earn money for
              food. I fetched water and washed their clothes.  My mother died in 2009 from an unknown illness
              that was very hard for her. My two sisters still live in the village. I am trying to help my
              youngest sister have a better life but presently I am not able to fund her to come to Freetown
              since I only can afford rent for a small apartment for myself.</p>
              <p>
              I am an industrious man who wants to create difference for myself in education to achieve a
              better future. I love kids and music so much; I started a music career at an early age. I
              started with western world music and later participated in the iEARN Positive Music Project
              (P.M.P) in 2007 and compiled an album. I began Volunteer working as an educator and
              joining many iEARN projects in Sierra Leone.</p>
              <p>
              When I finished secondary education, I started working with a few students to help educate
              them. In 2009, I joined the empowering children as a teacher. Because of the brutal and
              senseless war in Sierra Leone over the past ten years, thousands of children had to drop out of
              school and fight for their survival by doing different things such as prostitution, trading, and odd
              jobs, and many are homeless. I thought it was wise to start working with kids, because I do not
              want the mistake to repeat itself and I want to change their mindset for the better. Although
              education is valued in Sierra Leone, the children face many overwhelming obstacles that get
              in the ways of good education. In Sierra Leone, the majority of children who are attending
              government schools are coming to school hungry and are learning with very few materials
              provided for them.</p>
              <p>
              In 2014, I started an organization called The Children Foundation of Technology -Sierra
              Leone. Our mission is to foster relationship amongst children and youth within and outside
              Sierra Leone by enhancing the full participation of children in the development process in
              Sierra Leone and to alleviate suffering amongst children and youth through I.T and Digital
              Media Technology. Our children embraced this new digital age. We would like to share our
              world and learn about the world outside Freetown, Sierra Leone.
              I have also participated in different projects in the iEARN Forum and have worked
              collaboratively in iEARN learning circles four times, where I met Mrs. Gina too. 
            </p>
</Grid>

	<Navbar />

</Grid>   
</div>

     


		)
	}
}

export default AboutPage
 