import React from 'react';
import Axios from 'axios';
import ArticlePreview from '../components/ArticlePreview';
import Header from './Header';
import TopBar from './topbar';
import NavBar from './navbar';

class Blog extends React.Component{
	constructor(props){
		super(props);
		this.state = 
		{
			posts: []
		}
	}
	componentDidMount()
	{
		Axios
		.get(
				"http://public-api.wordpress.com/rest/v1/sites/cfotsl495492035.wordpress.com/posts"
			)
		.then(res => {
				this.setState({posts: res.data.posts });
				console.log(this.state.posts);
				}
			)
			.catch(error => console.log(error));

	}

	render()
	{
		return(
				<div >
					<Header />
					<TopBar />

					<div style = {{}}>
						<h1 style = {{textAlign: 'center'}}> Articles </h1>

						{this.state.posts.map(post => <ArticlePreview post = {post} /> )}
					</div>
					<NavBar />
				</div>
			)

	}
}

export default Blog;