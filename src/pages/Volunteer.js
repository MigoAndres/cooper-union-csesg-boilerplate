import React from 'react';
import Header from './Header';
import Grid from '@material-ui/core/Grid';
import TopBar from './topbar';
import Navbar from './navbar';

class Volunteer extends React.Component{
  render()
  {
    return(

		<div>	
    		<Header />		
    		<TopBar />

<Grid container spacing={24} >
        <Grid item xs={12} style={{marginRight:50, marginLeft:50}}> 


		<div>			

 	<Header />
          <TopBar />
<Grid container spacing={24} >
        <Grid item xs={12} style={{marginRight:50, marginLeft:50}}> 

		 	<h1>
		 		Volunteer
			</h1>	
			
			<p>
				The current plan for the Children’s Foundation of Technology - Sierra Leone is to have the school accommodate all the students and orphans. The Wyoming community has tried and is still trying to make a difference for the less-privileged kids in Sierra Leone. We want them to know that they are also part of the community, not outcasts or unwanted; they can make a difference too. That’s why the founder of CFOT thought a great way to show them was to start a new school for the children, less-privileged and orphans both, to learn I.T and digital media technology skills for free. With these skills, the children will be able to be self-dependent in the future. 
			</p>

			<p>
				Mr. Beard and the Wyoming Community support the idea and are ready to raise the new school budget. The doors are open if anybody wants to offer his or her own way of speeding up the building process. 
			</p>

			<p> 
				Sierra Leone has suffered and is still suffering from different diseases and disasters. Currently, thousands of students are dropouts and millions are homeless. For the past years, the CFOT School has been giving these deprived kids access to free computer learning- now we want to create room for the orphans too.
			</p>

			<p>
				However, you can support us in other ways as well. You can click the donate button to donate- both money and objects work. Objects would include things like computers, iPads, cameras, food, etc. As long as they are in good condition and are not personal affects, or aren't opened or individual packages, we accept all. Creating a fundraiser to help us is another way to help. 
				But most importantly, we would be thankful if you could spread the word about us. Thank you. 
			</p>

			  <Navbar position= "fixed" bottom= "0"/>
		
                
</Grid>
</Grid>

       </div>

		</div>
			  <Navbar position= "fixed" bottom= "0"/>         
</Grid>
</Grid>
		</div>
		)
	}
}

export default Volunteer;