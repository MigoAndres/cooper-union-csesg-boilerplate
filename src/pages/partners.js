

import React from 'react';
import Header from './Header';
import Grid from '@material-ui/core/Grid';
import TopBar from './topbar';
import Navbar from './navbar';


class Partners extends React.Component{
	render()
           {
		return(
			<div>
				<Header/>
				<TopBar/>

            
           <Grid container spacing={24}>
    
				<h1 style={{color:'black'}} > Our Partners </h1>
				<ul type="none">
					<li> <b> Wyoming City Schools </b>
          <p>Thanks to Mr Beard, Wyoming parent, staff and students and to all Wyoming
					people that come together to make the fundraiser a success. The goal for this fundraiser is to build a new school
					that can accommodate all the kids and also have rooms for orphans. One brick at a time, we will achieve it soon.
                    For the past years we have being having a great partnership and the CFOT School is doing well as more progress is going on
                    gradually in the school. With your continuous effort and support we will continue to do more here and make sure the free computer leaning 
                    spread in different communities in the country for other kids to benefit from it that are miles away from our current location. </p></li>
			       </ul>
             </Grid>
		 <Grid container spacing={100}>
			    <ul>
        <img src=  "https://cfotsl.weebly.com/uploads/2/7/1/1/27111981/sept-10-2017-21_orig.jpg"style={{width: 250, height: 200}}/>
        </ul>
                        <ul>
        <img src=  "https://cfotsl.weebly.com/uploads/2/7/1/1/27111981/sept-10-2017-8_orig.jpg"style={{width: 250, height: 200}}/>
        </ul>
                       <ul>
         <img src=  "https://cfotsl.weebly.com/uploads/2/7/1/1/27111981/sept-10-2017-23_orig.jpg"style={{width: 250, height: 200}}/>
        </ul>

    

                <li>
         <b>Jonathan Chin</b>
                     <p>Introduced Mr. Kanu to the Computer
                    Science Squad (CSS) to make their new CFOTSL website at
                    Cooper Union. Jonathan donated computers, cameras, and even
                    wires for the organization. Moreover, Jonathan also created
                    a paypal account for Mr. Kanu in order for the donor's money
                    to safely go to the Children's Foundation of Technology.{" "} </p>
                </li>

                <li>
            <b>Ann C. Gaudino</b>
            <p>
                 Ann received her doctorate degree in Education Administration and 
                 Policy Studies from the University of Pittsburgh, 
                 a Specialist degree in Education Administration from Wayne State 
                 University, and a
                 Master and Bachelor degrees in Music 
                 from the University of Michigan. Ann is 
                 currently an associate professor at Millersville University and teaches graduate and doctoral courses in 
                  education and education leadership to the next generation of teachers
                   and administrators. When she’s not in the classroom, Ann is the founder 
                   and editor of the Excellence in Education Journal, an online journal that
                    promotes and circulates scholarly educational writings in a public forum 
                    with no fee. Dedicated to supporting
                  teachers and students across the world, Ann’s commitment to transforming lives 
                  through education is inspiring, and for her—it all began at Ellis.
                 If you go into the link, https://www.conferenceoneducationandpoverty.org/
                 you can read more details for each section and schedules. The Conference 
                  Director, Ann C. Gaudino is currently sponsoring Mr. Kanu.
                </p>
                 </li>

                            <img
                                src="https://bbk12e1-cdn.myschoolcdn.com/ftpimages/389/news/large_news895836_891037.png"
                                style={{ width: 400, height: 400 }} />

                                <li>
                <b>Gina Denbow</b>
                <p>
                 Teacher At The Campobello Island Consolidated School- Canada ( They Provide
                  food for the students ). Their goal is to Cultivate the future: Campobello School is committed to educate, 
                 motivate, and empower all students to become lifelong productive members of a global society (email:ginaroxanne.denbow@nbed.nb.ca)
                 </p> </li>
      <li>
      <b>Amy Rogers</b>
      <p>
                AIG specialist at Silk Hope School/Bonlee Elementary School </p> </li>

	</Grid>

                <Navbar/>



           </div>
        );
    }
}


export default Partners;
